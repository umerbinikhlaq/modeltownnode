
var mongoose = require('mongoose');

//connect to MongoDB
mongoose.connect('mongodb://localhost/bit_college_db');
var db = mongoose.connection;

//handle mongo error
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
 console.log('Database Connected');
});


// define the schema for our user model
var userSchema = mongoose.Schema({

      username     : {type:String, trim:true, unique:true},
      password     : {type:String, trim:true}
  });

var user = mongoose.model('user', userSchema);



// define the schema for our chat model
var chatSchema = mongoose.Schema({
 room_id: String,
 user_name: String,
 user_email: String,
 messages:{
   sender_id: String,
   reciever_id: String,
   msg: String,
   date:{ type: Date, default:Date.now}
 }
});
var chat = mongoose.model('Message', chatSchema);


module.exports = {
  User:user,
  Chat:chat
}