var createError = require('http-errors');
var express = require('express');
var app = express();
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const session = require('express-session')({  
  cookie:{maxAge:3600000},
  secret: 'chatroom',
  resave: true,
  saveUninitialized: true});
var bodyParser = require('body-parser');
var passport = require('passport'),LocalStrategy = require('passport-local').Strategy;
var flash    = require('connect-flash');
var sharedsession = require("express-socket.io-session");
var io  = app.io = require( "socket.io" )();

var configFile = require('./config/database.js');
user = [];
connections = [];
rooms = [];


//define routes
 var indexRouter = require('./routes/index');
 var usersRouter = require('./routes/home');
 var loginRouter = require('./routes/login');

 // view engine setup
 app.set('views', path.join(__dirname, 'views'));
 app.set('view engine', 'ejs');
 app.set('socketio', io);

//app.usee
 app.use(logger('dev'));
 app.use(express.json());
 app.use(bodyParser.json());
 app.use(bodyParser.urlencoded({ extended: true }))
 app.use(express.urlencoded({ extended: false }));
 app.use(cookieParser());
 app.use(express.static(path.join(__dirname, 'public')));

 //use sessions for tracking logins
app.use(session);
io.use(sharedsession(session, {
  autoSave:true
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


//using passport
passport.use(new LocalStrategy({passReqToCallback : true},
    function(req, username, password, done) {
      configFile.User.findOne({ username: username }, function (err, user) {
        if (err) { return done(err); }
        if (!user) {
          return done(null, false, req.flash('message', 'Incorrect Username.'));
        }
        if (user.password != password) {
          return done(null, false, req.flash('message', 'Incorrect Password.'));
        }
        return done(null, user);
      });
    }
  ));

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  
  passport.deserializeUser(function(id, done) {
    configFile.User.findById(id, function(err, user) {
      done(err, user);
    });
});
 
 app.use('/', loginRouter);
 app.use('/index', indexRouter);
 app.use('/home', usersRouter);
 //app.use('/home/user_cred', usersRouter);


 
//socket chat
io.on('connection', function(socket){
  connections.push(socket);
  console.log('connected: %s sockets connected', connections.length);


  //get user back on page reload
  if(socket.handshake.session.loggedUser){
    io.sockets.emit('loaduser', socket.handshake.session.loggedUser);
    }
 
  //get users
  socket.on('new user', function(newUser){

    //save users into sessions
    socket.handshake.session.loggedUser = newUser;
    socket.handshake.session.save();
    console.log(socket.handshake.session)
    
    socket.username = newUser.user;
    socket.email = newUser.email;
    io.sockets.emit('users', {user:newUser.user, email:newUser.email});

    //retrieve old messages  
    var testDummy = socket.email.trim();
    testDummy.toString();
    configFile.Chat.find({"room_id":testDummy}).then(function(oldmessages){
      oldmessages.forEach(element => {
        io.sockets.emit('recent chat', {msg:element.messages.msg, user:element.user_name});
      })
    })
  })

  //remove user from session as well as from userlist
  socket.on('unloaduser', function(data){
    console.log(data);
    io.sockets.emit('removeUser', data);
    if (socket.handshake.session.loggedUser) {
      delete socket.handshake.session.loggedUser;
      socket.handshake.session.save();
     }
  })


 //create rooms
  socket.on('request from client', function(room){
    
    //generate room 
      socket.join(room.email); 
      //rooms.push(room.email);
  })

 //get user messages and display on both user and admin side
  socket.on('client message', function(client_message){

    //count msgs and showing on admin side
    console.log(client_message.user_msg);
    io.sockets.emit('count', {msg:client_message.user_msg, id:socket.email});

    //save to database
    var newMsg = new configFile.Chat({
      room_id: socket.email,
      user_name:client_message.user,
      messages : {msg:client_message.user_msg, sender_id:socket.id}
    });
    
    newMsg.save(function(err){
      if(err) throw err;
    });

    io.sockets.to(socket.email).emit('user_message', client_message);
    console.log('user msg to '+ socket.email)

  });


  //get user room id and return messages against queries
  socket.on('get room id',function(room_id){

    if(rooms.length > 1){
      socket.leave(rooms[0]);
      console.log(room_id)
      rooms.splice(1);
    }

    socket.join(room_id);
    rooms.push(room_id);
    console.log(rooms);
    // console.log('admin joins  '+ room_id)


    //retrieve old messages
    var testDummy = room_id.trim();
    testDummy.toString();  
    configFile.Chat.find({"room_id":testDummy}).then(function(oldmessages){
      oldmessages.forEach(element => {
        io.sockets.emit('chathistory', {msg:element.messages.msg, user:element.user_name});
      })
    })

    //get admin messages and display on both user and admin side
    socket.on('admin message', function(data){

    //save to database
    var newMsg = new configFile.Chat({
      room_id: testDummy,
      user_name:'admin',
      messages: {msg:data.data, reciever_id: socket.id}
    });
    
    newMsg.save(function(err){
    if(err) throw err;
    });

    io.sockets.to(room_id).emit('display',data);
    console.log('admin msgs are going to '+ room_id);
    })
 });


  //on disconnect
  socket.on('disconnect', function(){
    user.splice(user.indexOf(socket.username),1)
    console.log(user);
    connections.splice(connections.indexOf(socket),1);
    console.log('Disconnected: %s sockets connected',connections.length)
  })
})

  

//   //typing
//   socket.on('typing', function(data){
//       socket.broadcast.emit('typing',{user:socket.username});
//   })



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
