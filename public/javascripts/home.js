$(function () {

    var socket = io.connect('http://localhost:3000'); 
    var $user = $("#username");
    var $email = $("#email");
    var $message = $(".message_input");
    

    //hide user form and display chat screen
    $("#submitBtn").click(function(){
        socket.emit('new user',{user:$user.val(), email:$email.val()});
        $user.val();
        $("#userFormArea").hide();
        $("#msg").show();

        //generate room for chat
        socket.emit('request from client',{user:$user.val(), email:$email.val()});
       
    });

    //close chat window
    socket.on('loaduser', function(users){
        console.log(users);
        $('#clossWindow').click(function(){
            socket.emit('unloaduser', users);
            $('.chat_window').hide();
        })
    })

    //send messages from client side
    socket.on('user_message', function(print_message){
        $('.messages').scrollTop(100000000000);
        $('.messages').append(`
        <li class="message left appeared">
            <div class="avatar"></div>
            <div class="text_wrapper">
                <div class="text">`+print_message.user_msg+`</div>
            </div>
        </li>`);
        $('.message_input').val('');

    })

    
    //Display messsages coming from admin side
    socket.on('display', function(data){
        $('.messages').scrollTop(100000000000000000);
        $('.messages').append(`
        <li class="message right appeared">
            <div class="avatar"></div>
            <div class="text_wrapper">
                <div class="text">`+data.data+`</div>
            </div>
        </li>`);
    })

    
    //Retrieve old messages
    socket.on('recent chat', function(data){

        $('.messages').scrollTop(1000000000000);
        if(data.user == 'admin'){

            $('.messages').append(`
            <li class="message right appeared">
                <div class="avatar"></div>
                <div class="text_wrapper">
                    <div class="text">`+data.msg+`</div>
                </div>
            </li>`)
        }else{

            $('.messages').append(`
            <li class="message left appeared">
                <div class="avatar"></div>
                <div class="text_wrapper">
                    <div class="text">`+data.msg+`</div>
                </div>
            </li>`)
        }
       
    })

    


    //send messages via enter and click 
    $('.send_message').on('click', function(){
        socket.emit('client message', {user:$user.val() ,user_msg:$message.val()})
    });
    $message.keyup(function (e) {
        if (e.which === 13) {
        $('.send_message').click();
         }
    });

});