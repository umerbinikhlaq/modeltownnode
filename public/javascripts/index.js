

       

        $(function(){
            var socket = io.connect('http://localhost:3000');

            //send message by clicking or pressing enter 
            var $msg = $('#msg');
            $('#sendBtn').click(function(){
                socket.emit('admin message', {data:$msg.val()});
            })
            $('#msg').keyup(function (e) {
                if (e.which === 13) {
                    $('#sendBtn').click();
                 }
            });

            //onload page get user
            socket.on('loaduser', function(users){
                console.log(users);
                if(users){
                $('#userlist').append(`
                <li class='userListClass' id='` +users.email+ `'>
                <a href="#"><span class="chat-avatar-sm user-img"><img src="/images/user.jpg" alt="" class="img-circle"><span class="status online"></span></span><span class="clear-user">`+ users.user +`</span><input type="hidden" id="umail" value="`+ users.email+`"></a>
                </li>`);
                
                //get user messages window on click
                $('.userListClass').bind('click', function(){
                    $('.userListClass').removeClass('active');
                    var thisId = $(this).attr('id').trim();
                    var thisElement = $(this);
                    var thisName = thisElement.find(".clear-user").html();
                
                    $('.getUserName').text(thisName);
                    var thisTempElem = document.getElementById(thisId);
                    $(thisTempElem).toggleClass( "active" );
                     
                    $('.chats').empty();
                    socket.emit('get room id', thisId);
                    console.log(thisId);
                });
            }
        })

                  
            //get user's list
            socket.on('users', function(users){
                $('#userlist').append(`
                <li class='userListClass' id='` +users.email+ `'>
                <a href="#"><span class="chat-avatar-sm user-img"><img src="/images/user.jpg" alt="" class="img-circle"><span class="status online"></span></span><span class="clear-user">`+ users.user +`</span><input type="hidden" id="umail" value="`+ users.email+`"></a>
                </li>`);
                
                //get user messages window on click
                $('.userListClass').bind('click', function(){
                    $('.userListClass').removeClass('active');
                    var thisId = $(this).attr('id').trim();
                    var thisElement = $(this);
                    var thisName = thisElement.find(".clear-user").html();
                
                    $('.getUserName').text(thisName);
                    var thisTempElem = document.getElementById(thisId);
                    $(thisTempElem).toggleClass( "active" );
                     
                    $('.chats').empty();
                    socket.emit('get room id', thisId);
                    console.log(thisId);
                });
            });

            //show and count messages
            
            
                var count = 0;
            socket.on('count', function(msg){
                count++;
                $('#countSpan'+msg.id).empty();
                $("#userlist").children('#'+msg.id).find(".clear-user").after( `<span id="countSpan`+msg.id+`" class="badge bg-danger pull-right"></span>` );
                $('#countSpan'+msg.id).html(count);
            })

            //remove users from the list 
            socket.on('removeUser', function(data){
                if(data){
                    $("#userlist").children('#'+data.email).remove();
                    $('.chats').empty();
                    $('.getUserName').text('');

                }
            });

            //display client message
            socket.on('user_message',function(client_message){
                $('.chat-wrap-inner').scrollTop(100000000000);
                $('.chats').append(`
                    <div class="chat chat-right">
                        <div class="chat-body">
                            <div class="chat-bubble">
                                <div class="chat-content">
                                    <p>`+client_message.user_msg+`</p>
                                    <span class="chat-time">`+ timepicker() +`</span>
                                </div>
                            </div>
                        </div>
                    </div>`
                );
            })


            //Display admin message
            socket.on('display', function(data){
                $('.chat-wrap-inner').scrollTop(100000000000);
                $('.chats').append(`
                <div class="chat chat-left">
                    <div class="chat-avatar">
                        <a href="profile.html" class="avatar">
                            <img alt="John Doe" src="/images/user.jpg" class="img-responsive img-circle">
                        </a>
                    </div>
                    <div class="chat-body">
                        <div class="chat-bubble">
                            <div class="chat-content">
                                <p>`+data.data+`</p>
                                <span class="chat-time">`+ timepicker() +`</span>
                            </div>
                        </div>
                    </div>
                </div>
                ` );
                $('#msg').val('');
            })

            
            //get old messages
            socket.on('chathistory', function(data){

                $('.chat-wrap-inner').scrollTop(100000000000000);
                if(data.user == 'admin'){
                    $('.chats').append(`
                    <div class="chat chat-left">
                      <div class="chat-avatar">
                        <div class="chat-body">
                            <div class="chat-bubble">
                                <div class="chat-content">
                                    <p>`+data.msg+`</p>
                                    <span class="chat-time">`+ timepicker() +`</span>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>`
                    );

                }else{
                    $('.chats').append(`
                    <div class="chat chat-right">
                        <div class="chat-body">
                            <div class="chat-bubble">
                                <div class="chat-content">
                                    <p>`+data.msg+`</p>
                                    <span class="chat-time">`+ timepicker() +`</span>
                                </div>
                            </div>
                        </div>
                    </div>`
                    );
                };
            });


            //time function
            function timepicker(time) {
                var d = new Date();
                var h = d.getHours();
                var m = d.getMinutes();
                if(h >= 12){
                    h = h-12;
                    var format = ' pm';
                }else{
                    var format = ' am';
                }
                if(h < 10){
                    h = '0'+h;
                    
                }
                if(m < 10){
                    m = '0'+m;
                }
                return h + ":" + m + format;
            }
            
        })
  