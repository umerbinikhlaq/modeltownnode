var express = require('express');
var passport = require('passport'),LocalStrategy = require('passport-local').Strategy;
var router = express.Router();


//Get login page
router.get('/', function(req, res){
  res.render('login',{ message: req.flash('message') });
}
);

//Authentication
router.post('/login',
  passport.authenticate('local', { successRedirect: '/index',
                                   failureRedirect: '/',
                                   failureFlash: true })
);


module.exports = router;