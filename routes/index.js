
var express = require('express');
var router = express.Router();




/* GET home page. */
router.get('/', isLoggedIn, function(req, res) {
  res.render('index',{user : req.user.username});
});


router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});



// if the user is authenticated
function isLoggedIn(req, res, next) {

  // if user is authenticated in the session, carry on 
  if (req.isAuthenticated())
      return next();

  // if they aren't redirect them to the home page
  res.redirect('/');
}
module.exports = router;